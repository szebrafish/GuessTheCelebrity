package com.zebra.guessthecelebrity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    ImageView imageViewCelebrity;
    Button buttonChoice1;
    Button buttonChoice2;
    Button buttonChoice3;
    Button buttonChoice4;

    Random rand = new Random();
    int currentAnswer = 0;
    int currentPosition = 0;

    String html = "";
    String[] names = new String[100];
    String[] images = new String[100];

    Bitmap bitmapCurrentImage;

    String celebSite = "http://www.posh24.com/celebrities";

    public class DownloadTaskHtml extends AsyncTask<String, Void, String>{


        @Override
        protected String doInBackground(String... params) {

            String source = "";

            try {

                URL url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);

                int data = reader.read();

                while (data != -1){

                    char current = (char) data;

                    source += current;

                    data = reader.read();
                }

                return source;


            } catch (MalformedURLException e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "URL Error", Toast.LENGTH_SHORT).show();

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "No Internet Access", Toast.LENGTH_SHORT).show();

            }

            return null;
        }
    }

    public class DownloadTaskImages extends AsyncTask<String, Void, Bitmap>{


        @Override
        protected Bitmap doInBackground(String... params) {

            try {

                URL url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                InputStream in = urlConnection.getInputStream();

                Bitmap bitmapDownloading = BitmapFactory.decodeStream(in);

                return bitmapDownloading;


            } catch (MalformedURLException e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "URL Error", Toast.LENGTH_SHORT).show();

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "No Internet Access", Toast.LENGTH_SHORT).show();

            }

            return null;
        }
    }

    public String[] parseNames(String source){

        int counter = 0;
        String[] parsedNames = new String[100];

        Pattern pattern = Pattern.compile("alt=\"(.*?)\"/");
        Matcher matcher = pattern.matcher(source);

        while (matcher.find()) {

            parsedNames[counter] = matcher.group(1);
            counter++;
        }

        return parsedNames;
    }

    public String[] parseImages(String source){

        int counter = 0;
        String[] parsedImages = new String[100];

        Pattern pattern = Pattern.compile("<img src=\"(.*?)\" ");
        Matcher matcher = pattern.matcher(source);

        while (matcher.find()) {

            parsedImages[counter] = matcher.group(1);
            counter++;
        }

        return parsedImages;
    }

    public void loadScreen(){

        currentAnswer = rand.nextInt(100);
        currentPosition = rand.nextInt(4);
        int[] answers = new int[4];

        //Populates answers[], and cycles infinitely in case of duplicates
        for (int counter = 0; counter < 4; counter++){

            answers[counter] = rand.nextInt(100);

            if (answers[counter] == currentAnswer){

                counter--;
            }
        }

        //Download current celeb image and set to ImageView
        DownloadTaskImages downloadImage = new DownloadTaskImages();

        try {

            bitmapCurrentImage = downloadImage.execute(images[currentAnswer]).get();

        } catch (InterruptedException e) {
            e.printStackTrace();

        } catch (ExecutionException e) {
            e.printStackTrace();

        }

        imageViewCelebrity.setImageBitmap(bitmapCurrentImage);


        //Set button names
        if (currentPosition == 0){

            buttonChoice1.setText(names[currentAnswer]);

        } else{

            buttonChoice1.setText(names[answers[0]]);
        }

        if (currentPosition == 1){

            buttonChoice2.setText(names[currentAnswer]);

        } else{

            buttonChoice2.setText(names[answers[1]]);
        }

        if (currentPosition == 2){

            buttonChoice3.setText(names[currentAnswer]);

        } else{

            buttonChoice3.setText(names[answers[2]]);
        }

        if (currentPosition == 3){

            buttonChoice4.setText(names[currentAnswer]);

        } else{

            buttonChoice4.setText(names[answers[3]]);
        }

    }

    public void clickButton (View view){

        String clickedAnswer = view.getTag().toString();

        if (clickedAnswer.equals(Integer.toString(currentPosition))){

            Toast.makeText(this, "Correct!", Toast.LENGTH_SHORT).show();
        }
        else{

            Toast.makeText(this, "Wrong! The correct answer was ".concat(names[currentAnswer]), Toast.LENGTH_SHORT).show();
        }

        loadScreen();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageViewCelebrity = (ImageView) findViewById(R.id.imageViewCelebrity);
        buttonChoice1 = (Button) findViewById(R.id.buttonChoice1);
        buttonChoice2 = (Button) findViewById(R.id.buttonChoice2);
        buttonChoice3 = (Button) findViewById(R.id.buttonChoice3);
        buttonChoice4 = (Button) findViewById(R.id.buttonChoice4);

        // Download the HTML of the celeb site
        DownloadTaskHtml downloadHtml = new DownloadTaskHtml();

        try {

            html = downloadHtml.execute(celebSite).get();

        } catch (InterruptedException e) {
            e.printStackTrace();

        } catch (ExecutionException e) {
            e.printStackTrace();

        }

        // Parse names and image URL's
        names = parseNames(html);
        images = parseImages(html);

        //Begin
        loadScreen();

    }
}
